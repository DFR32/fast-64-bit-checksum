#include <cstdint>

#define HASH_ROUNDS 20

inline uint64_t fast_hash(const uint64_t& expr)
{
    uint8_t b_1 = expr & 0xFF;
    uint8_t b_2 = (expr >> 8) & 0xFF;
    uint8_t b_3 = (expr >> 16) & 0xFF;
    uint8_t b_4 = (expr >> 24) & 0xFF;
    uint8_t b_5 = (expr >> 32) & 0xFF;
    uint8_t b_6 = (expr >> 40) & 0xFF;
    uint8_t b_7 = (expr >> 48) & 0xFF;
    uint8_t b_8 = (expr >> 56) & 0xFF;

    uint8_t algorithm_state[8] = { 0xAE, 0x07, 0xCB, 0x97, 0x3E, 0x29, 0x83, 0x61 };

    for(uint64_t i = 0; i < HASH_ROUNDS; ++i)
    {
        b_1 = (((b_6 << 3) | (b_6 >> 5)) + b_1) & 0xFF;
        b_2 = (((b_4 << 3) | (b_4 >> 5)) + b_2) & 0xFF;
        b_3 = (((b_5 << 3) | (b_5 >> 5)) + b_3) & 0xFF;
        b_4 = (((b_8 << 3) | (b_8 >> 5)) + b_4) & 0xFF;
        b_5 = (((b_3 << 3) | (b_3 >> 5)) + b_5) & 0xFF;
        b_6 = (((b_1 << 3) | (b_1 >> 5)) + b_6) & 0xFF;
        b_7 = (((b_2 << 3) | (b_2 >> 5)) + b_7) & 0xFF;
        b_8 = (((b_7 << 3) | (b_7 >> 5)) + b_8) & 0xFF;

        b_1 += ((algorithm_state[b_8 % 8] << 5) | (algorithm_state[b_8 % 8] >> 3)) & 0xFF;
        b_2 += ((algorithm_state[b_7 % 8] << 5) | (algorithm_state[b_7 % 8] >> 3)) & 0xFF;
        b_3 += ((algorithm_state[b_6 % 8] << 5) | (algorithm_state[b_6 % 8] >> 3)) & 0xFF;
        b_4 += ((algorithm_state[b_5 % 8] << 5) | (algorithm_state[b_5 % 8] >> 3)) & 0xFF;
        b_5 += ((algorithm_state[b_4 % 8] << 5) | (algorithm_state[b_4 % 8] >> 3)) & 0xFF;
        b_6 += ((algorithm_state[b_3 % 8] << 5) | (algorithm_state[b_3 % 8] >> 3)) & 0xFF;
        b_7 += ((algorithm_state[b_2 % 8] << 5) | (algorithm_state[b_2 % 8] >> 3)) & 0xFF;
        b_8 += ((algorithm_state[b_1 % 8] << 5) | (algorithm_state[b_1 % 8] >> 3)) & 0xFF;

        b_1 ^= (b_3 << 5) | (b_3 >> 3);
        b_2 ^= (b_5 << 2) | (b_5 >> 6);
        b_3 ^= (b_8 << 3) | (b_8 >> 5);
        b_4 ^= (b_2 << 6) | (b_2 >> 2);

        b_5 ^= (b_1 << 3) | (b_1 >> 5);
        b_6 ^= (b_7 << 6) | (b_7 >> 2);
        b_7 ^= (b_4 << 5) | (b_4 >> 3);
        b_8 ^= (b_6 << 2) | (b_6 >> 6);
    }

    return (uint64_t)((uint64_t)b_1 | ((uint64_t)b_2 << 8) | ((uint64_t)b_3 << 16) | ((uint64_t)b_4 << 24) |
                      ((uint64_t)b_5 << 32) | ((uint64_t)b_6 << 40) | ((uint64_t)b_7 << 48) | ((uint64_t)b_8 << 56));
}